# Mixed integer ink selection
#### NAVID ANSARI, Max Planck Institute for Informatics, Germany
#### OMID ALIZADEH-MOUSAVI, Depsys SA, Switzerland
#### HANS-PETER SEIDEL, Max Planck Institute for Informatics, Germany
#### VAHID BABAEI, Max Planck Institute for Informatics, Germany
![teaser](teaser.PNG)
#### Codes and datasets of the paper "Mixed Integer Ink Selection for Spectral Reproduction"
In Dataset folder you can find the dataset ta we have used in this project (watercolor paints' reflectance, oil paints' reflectance etc.) 

In ink selection folder you will find the MATLAB code for solving the optimization using Gurobi and Yalmip and all the necessary data. 

Finally in spectral separation folder you will find the neural network models and the dataset for training them.
