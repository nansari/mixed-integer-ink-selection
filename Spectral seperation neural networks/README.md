# Neural network
 Spectral separation network consists of two different neural networks:
*	Forward model: Halftone to reflectance
*	Backward model: Reflectance to halftone

We should start by training the forward network first (HalftoneToSpec.py) then we freeze the weights of the forward and train the backward model(Forward+Backward.py).

Once you have trained the general backward model you start the adaptive learning (Adaptive learning.py), This will improve the loss of reproduction.

One the adaptive model is trained you can generate the halftone of the painting (generate_halftone.py).

The codes are tuned for reproducing the painting of the flower but you can load other paintings from the “Mixed-integer-ink-selection/Dataset/Spectral paintings/*.mat”
Due too the large size of the images we have to process them in smaller batches and attach them together later. 

The output of the generate halftone is a series of *.npy files containing the halftone of the painting in a n x 8 structure which you need to reshape later (\Prepare the area coverage to print\coverage2haftone.m) to get your 8 layers of halftone maps. Each for one ink channel in the printer. 
In order to test the effect of network capacity on this task we have trained a smaller network (small_forward_net.py) with 2 hidden layers each 50 perceptron wide. The difference in loss is insignificant, as a result we can replace this smaller network for the tasks where smaller capacityis an advantage.

In addition to this 8 Epson ink network you can find another network (Canon_Net.py). This is the forward model trained on a set of 4 Canon inks.
