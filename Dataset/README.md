# Dataset
Here you can find a range of datasets that we have used throughout this project: 
### Watercolor:
Here you can find the reflectance of 423 handmade watercolor batch.
![Dataset Watercolor](./Water%20color%20paint/all.png)
### Oil paint:
Here you can find the reflectance of 289 handmade oil paint batch.
![Dataset Oil paint](./Oil%20paint/all.png)
### Spectral paintings:
We have converted several paintings RGB to reflectance using nonlinear regression and a data set of 300 samples. Here you can find some of these spectral images. The images are reshaped into n x 31 array. 31 is the number of the samples in the wavelengths between 400 to 700 nm with 10 nm steps. 
### Dataset 300 samples:
Here you will find the reflectance and transmittance of 300 samples that is used for training the regression model. The samples are made with a R2880 Epson printer and its original inks. You can also find the halftone map of these samples that you can use to print them.
![Dataset 300 samples](./Dataset_%20300samples/Dataset300.png)
### Dataset 30000 samples:
Here you will find the RGB value of around 33000 samples that is used for training the neural network model. The samples are made with a R2880 Epson printer and its original inks. You can also find the halftone map of these samples that you can use to print them.
![Dataset 30000 samples](./Dataset_%2033000samples/all.png)
